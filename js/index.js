window.addEventListener('load', () => {
   let btnLogin = document.getElementById('btn-login');
   
   if (btnLogin) {
      let loginModal = document.getElementById('loginModal');
      let registerModal = document.getElementById('registroModal');
      let btnRegistro = document.getElementById('btn-registro');

      let btnCloseLogin = document.getElementById('closeLogin');
      let btnCloseRegister = document.getElementById('closeRegister');
      
      btnCloseLogin.addEventListener('click', (event) => {
         loginModal.style.display = 'none';
      });
      btnCloseRegister.addEventListener('click', (event) => {
         registerModal.style.display = 'none';
      });
   
      window.addEventListener('click', (event) => {
         if (event.target == loginModal || event.target == registerModal) {
            registerModal.style.display = 'none';
            loginModal.style.display = 'none';
         }
      })
      btnLogin.addEventListener('click', (event) => {
         event.preventDefault();

         console.log(loginModal.style.display);

         if (loginModal.style.display === 'flex') {
            loginModal.style.display = 'none';
            return;
         }

         if (registerModal.style.display === 'flex') {
            registerModal.style.display = 'none';
            return;
         }

         loginModal.style.display = 'flex';

         btnRegistro.addEventListener('click', (event) => {
            event.preventDefault();
            registerModal.style.display = 'flex';
            loginModal.style.display = 'none';
         });
      });
   }

   /*card en tiempo real*/

   if (document.getElementById('newEvent')) {
      let nameEvent = document.getElementById('newEvent').name;
      let priceEvent = document.getElementById('newEvent').price;
      let descriptionEvent = document.getElementById('newEvent').description;
      let imgEvent = document.getElementById('newEvent').image;

      let cardEdit = document.getElementById('card-edit');

      if (nameEvent) {
         let img = document.getElementById('img-card');
         let title = document.querySelector('.card.edit .card-title.text-dark');
         let price = document.querySelector('.card.edit .price p:not(.euro)');
         let description = document.querySelector('.card.edit .card-content p');

         imgEvent.addEventListener('change', (event) => {
            let file = event.target.files[0];
            let reader = new FileReader();

            if (file) reader.readAsDataURL(file);

            reader.addEventListener('load', e => {
               document.getElementById('img-card').src = reader.result;
            }, false);
         });
         console.log(description);
         nameEvent.addEventListener('input', (event) => {
            title.innerText = event.target.value;
         });
         priceEvent.addEventListener('input', (event) => {
            if (event.target.value.length < 4)
               price.innerText = event.target.value;
         });
         descriptionEvent.addEventListener('input', (event) => {
            console.log("a");
            description.innerText = event.target.value;
         });
      }
      console.log(cardEdit);
   }



   document.getElementById('ico-login').addEventListener('click', (event) => {
      event.preventDefault();
      if (loginModal.style.display === 'flex') {
         loginModal.style.display = 'none';
         return;
      }
      let registerModal = document.getElementById('registroModal');
      
      console.log(registerModal);
      if (registerModal.style.display === 'flex') {
         registerModal.style.display = 'none';
         return;
      }

      document.getElementById('loginModal').style.display = 'flex';

      document.getElementById('btn-registro').addEventListener('click', (event) => {
         event.preventDefault();
         document.getElementById('registroModal').style.display = 'flex';
         document.getElementById('loginModal').style.display = 'none';
      });
   });

});
